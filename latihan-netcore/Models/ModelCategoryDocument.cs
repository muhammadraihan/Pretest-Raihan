﻿using System.ComponentModel.DataAnnotations;

namespace latihan_netcore.Models
{
    public class ModelDocumentCategory
    {
        [Required]
        public int? CreatedBy { get; set; }

        [Required]
        public string? Name { get; set; }
        
        public DateTime Date { get; set; } = DateTime.Now;
    }
}
