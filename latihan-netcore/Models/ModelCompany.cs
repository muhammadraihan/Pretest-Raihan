﻿using System.ComponentModel.DataAnnotations;

namespace latihan_netcore.Models
{
    public class ModelCompany
    {
        [Required]
        public int? ID { get; set; }

        [Required]
        public string? Name { get; set; }

        public string? Address { get; set; }

        public string? Telephone { get; set; }

        [Required]
        public string? Email { get; set; }

        [Required]
        public int? CreatedBy { get; set; }

        public int? Flag { get; set; }

        public DateTime Date { get; set; } = DateTime.Now;
    }
}
