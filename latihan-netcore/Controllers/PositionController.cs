﻿using Dapper;
using latihan_netcore.Models;
using latihan_netcore.Repository;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System.Data;

namespace latihan_netcore.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class PositionController : Controller
    {
        private readonly IJWTAuthManager _authentication;
        public PositionController(IJWTAuthManager authentication)
        {
            _authentication = authentication;
        }

        /*[Microsoft.AspNetCore.Mvc.HttpPost("Login")]*/
        [HttpPost("Login")]
        [AllowAnonymous]
        public IActionResult Login([System.Web.Http.FromBody] LoginModel user)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest("Paramter is missing");
            }

            DynamicParameters dp_param = new DynamicParameters();
            dp_param.Add("email", user.Email, DbType.String);
            dp_param.Add("password", user.Password, DbType.String);
            dp_param.Add("retVal", DbType.String, direction: ParameterDirection.Output);

            var result = _authentication.Execute_Command<ModelUser>("sp_loginUser", dp_param);

            if (result.Code == 200)
            {
                var token = _authentication.GenerateJWT(result.Data);

                return Ok(token);
            }

            return NotFound(result.Data);
        }


        [HttpGet("PositionList")]
        [Authorize(Roles = "Admin")]
        public IActionResult getPosition()
        {
            var result = _authentication.getPositiontList<ModelPosition>();

            return Ok(result);
        }

        [HttpPost("Create")]
        /*[Authorize(Roles = "Admin")]*/
        public IActionResult Register([System.Web.Http.FromBody] ModelPosition user)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest("Parameter is missing");
            }

            DynamicParameters dp_param = new DynamicParameters();
            dp_param.Add("name", user.Name, DbType.String);
            dp_param.Add("iduser", user.CreatedBy, DbType.Int32);
            dp_param.Add("retVal", DbType.String, direction: ParameterDirection.Output);

            var result = _authentication.Execute_Command<ModelPosition>("sp_createPosition", dp_param);
            if (result.Code == 200)
            {
                return Ok(new { data = user });
            }

            return BadRequest(result);
        }

        [HttpPut("Update")]
        [Authorize(Roles = "Admin")]
        public IActionResult Update([System.Web.Http.FromBody] ModelPosition user, string id)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest("Parameter is missing");
            }

            DynamicParameters dp_param = new DynamicParameters();
            dp_param.Add("ID", id, DbType.String);
            dp_param.Add("name", user.Name, DbType.String);
            dp_param.Add("iduser", user.CreatedBy, DbType.Int32);
            dp_param.Add("retVal", DbType.String, direction: ParameterDirection.Output);

            var result = _authentication.Execute_Command<ModelPosition>("sp_updatePosition", dp_param);
            if (result.Code == 200)
            {
                return Ok(result);
            }

            return BadRequest(result);
        }

        [HttpDelete("Delete")]
        [Authorize(Roles = "Admin")]
        public IActionResult Delete(string id)
        {
            if (id == string.Empty)
            {
                return BadRequest("Parameter is missing");
            }

            DynamicParameters dp_param = new DynamicParameters();
            dp_param.Add("ID", id, DbType.String);
            dp_param.Add("retVal", DbType.String, direction: ParameterDirection.Output);

            var result = _authentication.Execute_Command<ModelPosition>("sp_deletePosition", dp_param);

            if (result.Code == 200)
            {
                return Ok(result);
            }

            return NotFound(result);
        }
    }
}

